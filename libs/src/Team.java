
import java.io.*;
import java.util.Arrays;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Team {

//	定义数组allurls用来存放url数据
	static String[] allurls = new String[999];
 //	定义allurls数组的长度
	static	int allurlsSize=0;
//  定义数组stuBase用来存放名字学号成绩的基本信息
	static Student[] stuBase = new Student[999];
//	定义stuBase数组的长度
	static int stuBaseSize=0;	
//  定义数组stuList用来存放按经验值正序排列后的学生名单
//ArrayList<Student> stuList = new ArrayList<>();
	static int aver=0; 
	static int createNum = 0;
	
	
  public static void main(String[] args) throws Exception {
	    // 读取配置文件
		String path = Team.class.getResource("config.properties").getPath();
		Properties prop = new Properties();
		prop.load(new FileInputStream(path));
		Document document = Jsoup.connect(prop.getProperty("url")).header("Cookie", prop.getProperty("cookie")).timeout(10000).get();
				

	  

  Elements mk = document.getElementsByClass("interaction-row");
  //遍历所有小作业模块的url存到allurls数组   
  for (Element p :  mk) {
		if (p.text().contains("课堂完成部分")) {
			getInfo(p, 1);
		}
	}

  

  /*for (int i = 0; i < mk.size(); i++) {	
		// 测试获取是否正确
		// System.out.println(rows_ChildElement.child(1).child(0).select("span").get(1).toString());
		if (mk.select("span").get(i).toString().contains("课堂完成")) {
			allurls.add(mk.attr("data-url"));
			//System.out.println(mk.attr("data-url"));
		}
	}
  */
  
//	获取总成员数
	String memNum = document.getElementById("menu-content-box").select("a").get(1).select("span").get(1).text();
	 stuBaseSize = Integer.parseInt(memNum.substring(1, memNum.length() - 1));
	 stuBase=new Student[stuBaseSize];
 
// 	将一个网页中每个人的情况分成一个个div小块，然后获得各个人的学号，姓名，经验值，并存入集合
	 for (int i = 0; i < allurlsSize; i++) {
			Document student = Jsoup.connect(allurls[i]).header("Cookie", prop.getProperty("cookie")).timeout(10000)
					.maxBodySize(0).get();
			Elements stuExp = student.getElementsByClass("homework-item");
			for (Element p : stuExp) {
				getInfo(p, 2);
			}
		}
//	实现排序\最高分、最低分、平均分\保存为txt文件
			Arrays.sort(stuBase);
			File txt = new File("score.txt");
			PrintWriter out = new PrintWriter(new FileWriter(txt));
			String first = "最高经验值为:"+stuBase[0].getExp()+"，"+"最低经验值为:"+stuBase[stuBaseSize-1].getExp()+"，"+"平均经验值为:"+aver/stuBaseSize;
			out.print(first+"\n");
			for(Student stu:stuBase)
				out.print(stu.toString()+"\n");
			out.close();
			}
			
	 public static void getInfo(Element p, int y) {
			if (y == 1) {
				String url = sort(p, "data-url=\"(\\S+)\"");
				 allurls[ allurlsSize] = url.replace("&amp;", "&");
				 allurlsSize++;
			} else if (y == 2) {
				// 获取姓名、 学号、经验
				int exp = Integer.parseInt(sort(p, "(\\d+) 分</span>"));
				p = p.getElementsByClass("member-message").first();
				String name = sort(p, "color: #333;\">(\\S+)</span>");
				String idTxt = p.select("div").last().text();
				int id = 0;
				if(!idTxt.equals(""))
					id = Integer.parseInt(idTxt);
				// 将信息存入对象数组中
				int stuNo = -1; 
				
				if(!name.equals("")){
					// 对象数组全为空的时候，先放入一个Student对象
					
					if (createNum == 0) {
						stuBase[createNum] = new Student(name, id, exp);
							
						aver += exp;
						createNum++;
					} else {
						stuNo = find(id);// 查询其是否在对象数组中，并记录其下标的值，不存在于数组中为-1
						// 未在对象数组中的，将其加入数组中
						if (stuNo == -1) {
							stuBase[createNum] = new Student(name, id, exp);
							aver += exp;
							createNum++;
						}
						// 在数组中的，则更新他的经验值
						else {
							stuBase[stuBaseSize].setExp(exp + stuBase[stuBaseSize].getExp());
							aver += exp;
						}
					}
				}
			}
		}

		// 通过正则表达式获取想要的信息
		public static String sort(Element p, String data) {
			String txt = p.toString();
			Pattern number = Pattern.compile(data);
			Matcher matcher = number.matcher(txt);
			String x = "";
			if (data.equals("(\\d+) 分</span>"))
				x = "0";
			while (matcher.find()) {
				x = matcher.group(1);
			}
			return x;
		}

		// 通过ID在对象数组查找其下标（在数组中的位置）
		public static int find(int id) {
			int isExit = -1;
			for (int x = 0; x < createNum; x++) {
				if (stuBase[x].getId()==id)
					isExit = x;
			}
			return isExit;
		}
}
		

	 
	 
	 

	

 




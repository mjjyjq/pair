public class Student implements Comparable<Student>{
	private String name;
	private int id;
	private int exp;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getExp() {
		return exp;
	}
	public void setExp(int exp) {
		this.exp = exp;
	}
	public String toString() {
		return  id + ", " + name + ", " + exp;
	}
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + exp;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (exp != other.exp)
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	public Student(){
		super();
	}
	public Student(String name,int id,int exp){
		this.name = name;
		this.id = id;
		this.exp = exp;
	}
	public int compareTo(Student o) {
		int sort1 = o.exp-this.exp;
		int sort2 = sort1 == 0 ? this.id-o.id : sort1;
		return sort2;
	}
}